#!/usr/bin/python

'''
This file is meant to serve as the controller file for our simulated system.  It
will initialize the objects of our system, run the event-based timeflow of the
system, and gather the results to be analyzed.
'''

import sys
import parse_input, parse_output, plot_time_trace
import event_queue

def controller(filename):
    '''Controller script that does it all, given a filename as our input.'''
    
    # First, create our queue
    queue = event_queue.EventQueue()
    
    # Next, parse the data from the input file
    input_data, interest, limit = parse_input.parse_file(filename)
    
    # Then initialize our objects using this input data, passing each our queue
    hosts, routers, links, flows = parse_input.initialize_objects(queue, 
                                                                  *input_data)
    
    # Keep track of which flows aren't finished yet
    running_flows = set(flows.keys())

    # We can start by having the routers start doing bellman-ford
    for r in routers.values():
        r.kick_off()
        
    # Then we let flows add to the queue
    for f in flows.values():
        # Each flow's kickoff method will add stuff to the queue
        f.kick_off()

    # keep track of how many events we run, for logging purposes
    counter = 0

    # Now, we just walk through the queue, one event at a time.  We end the loop
    # when one of three things happens:
    # (1) The event queue is empty (we've run out of events to run)
    # (2) The set of running flows is empty (all flows have finished sending
    # their data)
    # (3) We hit our time limit, if one is specified (NOTE: this end condition
    # happens within the loop itself, to avoid accidentally running one event
    # past the limit)
    while not queue.is_empty() and len(running_flows) > 0:
        # get the first item in the queue
        timestamp, function = queue.remove()
        
        # if we have a limit and this new time passes it, don't run the function
        # at all (and instead just stop immediately)
        if 0 <= limit < timestamp:
            print "Time Limit Reached: %s seconds" % limit
            break
        
        # set the time of the system
        queue.set_time(timestamp)
        
        # keep track of how many events we run
        counter += 1
        
        # every 5000 events, print out how far we've gone (for the benefit of
        # the bored viewer waiting at a terminal)
        if counter % 5000 == 0:
            print "%s: \t%s" % (counter, timestamp)
        
        # before any action happens, note the current state of our measured data
        # (note that this state has been constant since the last action was
        # finished, at the previous timestamp)
        parse_output.get_stats(timestamp, hosts, routers, links, flows, interest)
        
        # The event at hand will either be a string, giving us information about
        # the state of the system, or a lambda expression serving as a wrapper
        # for code we wish to execute as our event
        if (type(function) is str):
            # if the given "function" is actually a string, then it's actually
            # just the flowID of a flow which has finished running (in which
            # case we should note that it's done)
            if function in running_flows:
                running_flows.remove(function)
                print "Flow %s has Finished!" % function
        else:
            # run the code in question (actually just a lambda expression)
            function()

    # now we're done running the simulation, and can process the results
    
    # use input filename to create corresponding output filename, for time trace
    # data collection
    if filename[-3:] == ".in":
        outname = filename[:-3]
    else:
        outname = filename
    outname = "results/" + outname
    outname_overall = outname + "_overall.out"
    outname_time = outname + "_time_trace.out"
    
    # output overall average data to tab-separated file, using output name
    # created above
    parse_output.output_overall_average(outname_overall)
    
    # output time trace data to tab-separated file for processing, using output
    # filename created above and sampling rate of 100 ms
    parse_output.output_time_trace(outname_time, 0.1)
    
    # process time trace data into pretty graphs
    plot_time_trace.plotfile(outname_time)

# We can run this file from the command-line, with the sole argument being an
# input filename.
if __name__ == '__main__':
    args = sys.argv
    if len(args) < 2:
        raise Exception("need second argument: filename to read from")
    filename = args[1]
    controller(filename)
