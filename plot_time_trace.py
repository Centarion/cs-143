#!/usr/bin/python

import os, os.path, sys
import parse_output
import matplotlib.pyplot as plt

# Get the list of attributes which parse_output has gathered
attributes = {}
for (key, valdict) in parse_output.attributes.iteritems():
    for val in valdict.keys():
        attributes[val] = []

# For any given attribute, provide the label for the y-axis of its corresponding
# graph
attlabels = {}
attlabels['link_buffer_occupancy'] = 'buffer occupancy (bytes)'
attlabels['link_packet_loss'] = 'number of packets lost (packets)'
attlabels['link_flow_rate'] = 'link rate (bytes per second)'
attlabels['flow_send_rate'] = 'flow send rate (packets per second)'
attlabels['flow_receive_rate'] = 'flow receive rate (packets per second)'
attlabels['flow_packet_rtt'] = 'flow packet round-trip time (seconds)'
attlabels['flow_window_size'] = 'flow window size (packets)'
attlabels['host_send_rate'] = 'host send rate (packets per second)'
attlabels['host_receive_rate'] = 'host receive rate (packets per second)'

def plotfile(filename, sep="\t"):
    '''Given the name of an time trace output file, read the data from this file
    and use it to plot the results.  As an optional argument, include the
    separating character used '''
    
    # Let the person sitting at the terminal know what's happening
    print "Graphing Time Trace Plots"
    
    # Determine the directory where the plots will go, creating it if necessary
    dirname = "results/" + filename.replace("/", "_").replace(".", "_") + "/"
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    # Read the datafile
    with open(filename, "r") as f:
        for line in f:
            if line[-1] == "\n":
                line = line[:-1]
            # The datafile has a separation character
            data = line.split(sep)
            
            # The first column is the name of the attribute which the data
            # corresponds to
            name = data[0]
            
            # The second column is the ID of the object which the data is from
            objID = data[1]
            
            # The third column is the sampling rate of the data in question (for
            # the time trace)
            rate = float(data[2])
            
            # The rest of the columns are the values at each timestep (the
            # y-coordinates of our plotted data)
            vals = [float(x) for x in data[3:]]
            
            # Create the corresponding x-coords for our data
            ratenums = [i * rate for i in range(1, len(vals) + 1)]
            
            # Store all data from the same attribute together
            attributes[name].append((objID, ratenums, vals))
            
    # Let's make the legends of our graphs actually be in order and look nice
    for name in attributes:
        attributes[name].sort()

    # For each attribute, plot our data for each object in a single graph
    for (attname, data) in attributes.iteritems():
        plt.clf()
        plt.figure(None, [15, 6])
        labels = []
        for (objID, ratenums, vals) in data:
            plt.plot(ratenums, vals)
            labels.append(objID)
        plt.xlabel("time (seconds)")
        plt.ylabel(attlabels[attname])
        plt.legend(labels, loc='best')
        
        # Turn the attribute name into a nice graph title
        title = " ".join([x.capitalize() for x in attname.split("_")])
        title = title.replace("Rtt", "RTT")  # special case for RTT
        plt.title(title)
        
        # Save the graph to an image, rather than showing it
        plt.savefig(dirname + attname + ".png")
        
        # If we wish to show the graph anyways, uncomment this line
        #plt.show()
        
if __name__ == '__main__':
    # actually run the code when we do command line stuffs
    args = sys.argv
    if len(args) < 2:
        print '''Usage: ./plot_time_trace.py <filename>\n
        Will turn time trace output file into graphs'''
        raise ValueError("incorrect number of arguments")
    filename = args[1]

