#!/usr/bin/python

'''
This file contains the Link object and its methods.  Recall that each Link
object is one-directional, and so each actual link will have two separate
objects created to represent it.
'''

class Link(object):

    def __init__(self, linkID, queue, info):
        '''Initializes the link object.  Is given the link's ID (a string), the
        queue object, and info relating to the link, and stores this
        information.
        
        The info variable is a 5-tuple, with the following elements:
        [0] The host/router ID of our source (string)
        [1] The host/router ID of our destination (string)
        [2] Link rate, in megabits per second (float)
        [3] Link delay, in milliseconds (float)
        [4] Link buffer, in kilobytes (float)
        '''
        self.linkID = linkID
        #Link endpoints 
        self.sourceID = info[0]
        self.destID = info[1]
        #Information about the link
        self.rate = info[2] * (125000) #stored in bytes/second
        self.delay = info[3] / 1000 #stored in seconds
        self.buff_size = info[4] * 1024 #stored in bytes
        #setup the internal buffer
        self.buff = []
        #setup statistics about our link
        self.buff_occ = 0
        self.dropped_packets = 0
        self.data_through = 0
        #Set up our reference to the event queue
        self.event_queue = queue
    
    def include_endpoints(self, endpoints):
        '''Given the hosts and routers of our system (in one large dictionary,
        with host/router ID strings as keys and the corresponding host/router
        objects as values), store references to the relevant hosts/routers in
        our object.'''
        self.source = endpoints[self.sourceID]
        self.dest = endpoints[self.destID]

    def queue(self, packet):
        '''
        This public function takes a packet and adds it to the internal
        buffer. This will perform all actions needed to get the packet
        to the end of the link and store the information in the packet.
        '''
        #if the packet does not cause a buffer overflow add it to the
        #buffer and start the sending rocess (if it is not already
        #in progress)
        if not (self.buff_occ + packet.size) > self.buff_size:
            self.buff.append(packet)
            self.buff_occ += packet.size
            if len(self.buff) == 1:
                self._dequeue()
        else:
            #drop the packet, tell me if data collection needs to be
            #notified/the data in the packet needs to be rescued
            self.dropped_packets += 1 

    def _dequeue(self):
        '''
        Private function called by queue and itself to determine
        when elements have finished being loaded through the link
        buffer.
        '''
        if len(self.buff) != 0:
            loading = self.buff[0].size / self.rate
            self.event_queue.add(loading, lambda: self._send())
            
    def _send(self):
        '''
        Private funtion called by dequeue to remove packets from the 
        buffer and call node.receive on the endpoint.
        '''
        if len(self.buff) != 0:
            p = self.buff.pop(0)
            self.buff_occ -= p.size
            self.data_through += p.size
            self.event_queue.add(self.delay, lambda: self.dest.receive(p))
            self._dequeue()
            
    def get_stats(self):
        '''
        Called by data output to get information about the link
        '''
        return self.buff_occ, self.dropped_packets, self.data_through
