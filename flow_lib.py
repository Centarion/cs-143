#!/usr/bin/python

'''
This file contains the Flow object and its methods.
'''

''' 
    TCP Tahoe:
        Congestion window W
        Max segment size: Max payload of a packet
        ssthresh = threshold to enter congestion avoidance state
        SLOW START state:
        Initialized to 2
        Every packet acknowledged, congestion window += 1

        CONGESTION AVOID:
        W += 1 every RTT

        In general:
        if nonsensical ack received -- a duplicate, most likely --
            treat as a timeout.
        If an ack times out, peform slow start by setting ssthresh 
            to half current congestion window and setting window to
            2

    FAST TCP:
        Estimate [observed RTT], [RTT given empty queue]
            (empty queue is minimum observed RTT seen ever)
        W <- W * (RTT empty) / (RTT) + alpha
            once per good packet ack
'''

import random

#enum for different control mode states
TAHOE_SLOW_START = 0
TAHOE_CONGEST_AVOID = 1
FAST_TCP = 10
FIXED = 100

class Flow(object):

    def __init__(self, flowID, queue, info):
        '''Initializes the flow object.  Is given the flow's ID (a string), the
        queue object, and info relating to the flow, and stores this
        information.
        
        The info variable is a 5-tuple, with the following elements:
        [0] The host ID of our source (string)
        [1] The host ID of our destination (string)
        [2] Data amount, in megabytes (float)
        [3] Flow start time, in seconds (float)
        [4] Flow control type of our flow.  Either a string denoting a specific
        algorithm (i.e. 'TCP-TAHOE' or 'FAST-TCP') or a nonnegative integer
        denoting a fixed window size.
        '''
        self.flowID = flowID
        self.sourceID = info[0]
        self.destID = info[1]
        self.size = info[2]
        self.startTime = info[3]
        self.algorithm = info[4]
        self.queue = queue


        # Debug: use these to force to use a particular
        # mode
        #self.algorithm = 'FIXED'
        #self.algorithm = 'TCP-TAHOE'
        #self.algorithm = 'FAST-TCP'

        ## General useful logging info
        # Total packets we've ever sent
        self.packets_sent = 0;
        # # of data-matching acks that have come back.
        self.packets_confirmed = 0
        # Number of dropped packets
        self.dropped_packets = 0;
        self.dropped_packets_ids = [];
        # Dictionary of packets currently in the air
        #  will contain their sending time and a locally unique id
        self.packets_in_air = {}

        # Basic parts of flow control
        # Max packets in air at a time
        self.window_size = 1
        # Multiples of the estimated rtt we'll consider to be "dropped"
        self.drop_time_mult = 2
        # Observed curent rtt
        self.avg_rtt = 0
        self.avg_rtt_time_constant = 0.99
        # Minimum ever rtt
        self.min_rtt = 0
        # keep time of last time drop penalty was applied
        # this is our response time, don't double-count failures
        self.last_drop_penalty_time = -100.0
        self.penalty_delay_time = 0
        # Big penalty safety window, as the network might take a while
        # to recover from congestion.
        self.penalty_delay_safety = 1.5
        # And time management of once-per-rtt updates
        self.last_once_per_rtt_update = -100.0

        ## FIXED WINDOW SIZE
        if (type(self.algorithm) != type('')):
            self.window_size = self.algorithm
            self.control_mode = FIXED

        ## TCP TAHOE SPECIFICS    
        if (self.algorithm == 'TCP-TAHOE'):
            # start off window size at 2
            self.window_size = 2
            # slow start icnrement amount
            self.slow_start_k = 0.5
            # collision avoid inc amount
            self.collision_avoid_k = 1.0
            # define slow start threshold to be pretty high to start with
            self.ssthresh = 500
            # current control mode is slow start
            self.control_mode = TAHOE_SLOW_START

        ## FAST TCP SPECIFICS
        elif (self.algorithm == 'FAST-TCP'):
            # start off window size at 1
            self.window_size = 1
            # window size smoothing factor over time
            self.window_size_smoothing = 0.9
            # alpha, constant factor pushing window size up
            self.alpha = 1.0
            # alpha learning rate, up
            self.alpha_gain = 0.000
            # alpha decay rate on error
            self.alpha_decay = 1.0
            # current control mode is fast_tcp
            self.control_mode = FAST_TCP

        elif (self.algorithm == 'FIXED'):
            self.control_mode = FIXED
            self.window_size = 50

    def include_endpoints(self, hosts):
        '''Given the hosts of our system (a dictionary, with host ID strings as
        keys and the corresponding host objects as values), store references to
        the relevant hosts in our object.'''
        self.source = hosts[self.sourceID]
        self.dest = hosts[self.destID]

    def kick_off(self):
        '''At the start of the simulation, add to the event queue events for 
        this flow telling a host to send data.'''
        self.queue.add(self.startTime, lambda: self.kick_host(True))

    def adjust_for_dropped_packet(self):
        '''Adjusts in the case of a dropped packet. Helper for kick_host
        and handling ack.'''
        if self.control_mode == TAHOE_SLOW_START or \
           self.control_mode == TAHOE_CONGEST_AVOID:
            # Only penalize permanently if this is the first drop in
            # in round trip time (estimated conservatively)
            if self.queue.get_time() - self.last_drop_penalty_time > self.penalty_delay_time:
                self.ssthresh = self.window_size / 2.0
                self.last_drop_penalty_time = self.queue.get_time()
                self.penalty_delay_time = self.penalty_delay_safety * self.drop_time_mult * self.avg_rtt
                print self.flowID, " penalized at t %f, rtt %f, w %d, ssthresh %f" % (self.queue.get_time(), self.avg_rtt, self.window_size, self.ssthresh)
            # But always penalize window size. 
            self.window_size = 2
            self.control_mode = TAHOE_SLOW_START
        elif self.control_mode == FAST_TCP :
            # Only penalize permanently if this is the first drop in
            # in round trip time (estimated conservatively)
            if self.queue.get_time() - self.last_drop_penalty_time > self.penalty_delay_time:
                # Back off alpha
                self.alpha *= self.alpha_decay
                self.last_drop_penalty_time = self.queue.get_time()
                self.penalty_delay_time = self.penalty_delay_safety * self.drop_time_mult * self.avg_rtt
                print self.flowID, " penalized at t %f, rtt %f, w %d, alpha %f" % (self.queue.get_time(), self.avg_rtt, self.window_size, self.alpha)



    def kick_host(self, kick_more):
        '''Called when the flow will attempt to send more packets.
        Kicks the appropriate host to start sending data. Boolean
        argument kick_more specifies whether this event should spawn more
        in the future if not all packets could be sent. Acts according
        to whatever flow control has been specified for this flow.'''

        # First check for dropped packets, that is packets that were sent
        # more than drop_time ago without an ack
        to_drop = []
        for k, v in self.packets_in_air.iteritems():
            if (self.avg_rtt > 0):
                drop_time_curr = self.drop_time_mult * self.avg_rtt;
            else:
                # We don't have a handle on rtt yet, use a guess
                drop_time_curr = 0.5;

            if (self.queue.get_time() - v > drop_time_curr):
                to_drop.append(k)

        if (len(to_drop) > 0):
            self.dropped_packets += len(to_drop)
            # Dropped packets aren't in the air
            for k in to_drop:
                self.packets_in_air.pop(k)
                self.adjust_for_dropped_packet()
                self.dropped_packets_ids.append(k)

        # Do things that need to be done every rtt
        # (nothing right now)
        if (self.queue.get_time() - self.last_once_per_rtt_update >= self.avg_rtt):
            # Adjust flow control for dropped packets
            if self.control_mode == TAHOE_SLOW_START \
                or self.control_mode == TAHOE_CONGEST_AVOID:
                # Nothing for these
                pass
            elif self.control_mode == FAST_TCP:
                # This got moved to per-ack basis, nothing here
                pass

        while (self.packets_confirmed + len(self.packets_in_air) < 1024*self.size and 
               len(self.packets_in_air.keys()) < self.window_size):
            #Sent a packet, keep count
            self.packets_sent += 1            
            # Use packets sent as a unique id, as it's constantly increasing
            self.source.send(self.packets_sent, self.destID, self)
            # Keep count that we sent
            self.packets_in_air[self.packets_sent] = self.queue.get_time();


        # And also plan a check it a little while to try to send more
        # in case we lose packets and no acks come back
        if (kick_more and self.packets_confirmed < 1024*self.size):
            if (self.avg_rtt == 0):
                self.queue.add(0.01, lambda: self.kick_host(True))
            else:
                self.queue.add(self.avg_rtt/self.window_size, lambda: self.kick_host(True))

    def got_ack(self, p_id):
        '''Called when we get an ack. Determines if that acknowledgement is
        good, or duplicate, and continues to kick the host.'''

        # Weird ack?
        if (p_id in self.packets_in_air):
            # Good ack.

            # Update rtt info
            this_rtt = self.queue.get_time() - self.packets_in_air[p_id]
            if self.avg_rtt == 0:
                self.avg_rtt = this_rtt
            else:
                # Avg it in
                self.avg_rtt = (self.avg_rtt * self.avg_rtt_time_constant + 
                              this_rtt * (1.-self.avg_rtt_time_constant))
            if (self.min_rtt == 0 or self.min_rtt > self.avg_rtt):
                self.min_rtt = self.avg_rtt

            # record packet arrival info
            self.packets_confirmed += 1;
            self.packets_in_air.pop(p_id)

            # Manage flow control
            if self.control_mode == TAHOE_SLOW_START:
                # In slow start, just increment window linearly
                self.window_size += self.slow_start_k;
                # If it becomes greater than ssthresh, go to congest avoid
                if (self.window_size > self.ssthresh):
                    self.control_mode = TAHOE_CONGEST_AVOID

            elif self.control_mode == TAHOE_CONGEST_AVOID:
                # Increment such that window_size += 1 in a whole
                # RTT
                self.window_size += self.collision_avoid_k/self.window_size

            elif self.control_mode == FAST_TCP:
                if (self.avg_rtt > 0):
                    new_window_size = self.window_size*self.min_rtt/self.avg_rtt \
                                       + self.alpha
                    self.window_size = (self.window_size_smoothing*self.window_size) + \
                                        new_window_size*(1 - self.window_size_smoothing)
                self.alpha += self.alpha_gain

        else:
            # Bad ack! Treat as a timeout if it's not for a packet that we sent and
            # considered dropped; in those cases, we underestimated drop time and
            # shouldn't consider it a penalty.
            if (p_id not in self.dropped_packets_ids):
                print self.flowID, " received a bad ack?"
                self.dropped_packets += 1
                self.adjust_for_dropped_packet()

        # If that makes us done, send a done indication. Otherwise
        # kick out more packets.
        if (self.packets_confirmed >= 1024*self.size):
            #Send indication that this flow is done.
            elapsed = self.queue.get_time() - self.startTime
            print "%s: Total dropped packets: %d/%d = %f percent" % (self.flowID, self.dropped_packets, self.packets_sent, 100.0*float(self.dropped_packets)/float(self.packets_sent))
            print "%s: Finished in %f second at avg rate %f Mbps" % (self.flowID, elapsed, 8.0*self.size/elapsed)
            self.queue.add(0.0, self.flowID);
        else:
            self.kick_host(False);

    def get_stats(self):
        '''Returns the flow's observations of its performance:
                -Total # of packets sent, inc. dropped
                -Total # of packets confirmed to be received (ack'd)
                -Observed avg packet rtt
                -Current window size
        '''
        return self.packets_sent, self.packets_confirmed, self.avg_rtt, self.window_size
