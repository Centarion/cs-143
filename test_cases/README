This is the README file to describe how inputs to the system work (such as
these test cases).  For more information, compare the test cases to the test
case description PDF.

(*) All files will be tab-separated.

(*) Any blank line will be ignored.  Additionally, if # (the standard Python
comment symbol) is present in a line, then that character and all characters
after it in the given line will be ignored (read as: commenting works).

(*) If you wish for the simulation to have a time limit, use a line which has
"limit" as its first column and a floating-point value (denoting the time at
which the simulation should stop) as its second column.  There can be at most
one of these lines; if no such line is listed, the simulation will run to
completion (i.e. when there are no more events in our system and/or when all
flows finish their data transfer).

(*) If you only care about certain links (so-called "links of interest"), list
them in a line which has "interest" as its first column and all such links in
the later columns.  There can be at most one of these lines; if no such line is
listed, all links will be treated as "interesting".

(*) If there are any hosts in the system, there must be a line which has
"hosts" as its first column, followed by an ID for each host in each later
column.  For routers, the same thing must apply, starting with "routers" as the
first column.  If there aren't any hosts, you may exclude the hosts line
altogether; the same goes for routers.  These lines, if they exist, must come
before any link or flow specification lines - for simplicity, it is recommended
to always place them at the top of the file.

(*) All other lines will be link-specification lines or flow-specification
lines:

(**) Link-specification lines start with "link" as the first column.  The next
column is the link ID, and the two following columns are the two objects (hosts
or routers) which the link has as endpoints.  The fifth column is the link
rate (in Mbps), the sixth is the link delay (in ms), and the seventh is the
link buffer size (in KB).

(**) Flow-specification lines start with "flow" as the first column.  The next
column is the flow ID, and the two following columns are the source and
destination hosts of the flow (in that order).  The fifth column is the amount
of data (in MB), and the sixth column is the start time of the flow (in s).
The seventh column is the flow control type for the flow in question - can
either be a string denoting the algorithm in question, or a nonnegative integer
denoting a fixed window size (with the integer being the size).

The flow algorithms currently accepted are:
FAST-TCP
TCP-TAHOE
