#!/usr/bin/python
'''
This file contains the Packet object. The Packet is a data container
class with no methods, it stores header information, a payload,
and data for tracing its route through the network.
'''

class Packet(object):

    def __init__(self, t, p_id, header, source, sendTime, flow, size, 
                 ack_requested = True, routingInfo=None):
        '''Initializes the packet object, given the header information for the
        packet as well as its size.'''

        #The type is one of the strings "routinginfo", "ack" or "data"
        #this specifies what nodes should do with a packet
        self.type = t
        
        #A unique id for the packet (unique within flow)
        self.p_id = p_id
        
        #The header stores a target hostID (we can make this any format
        #we need for the router to read it)
        self.header = header
        
        # The source stores the source hostID
        self.source = source

        # The flow stores the flow that generated this packet
        self.flow = flow
        
        #This stores the packet size in bytes
        self.size = size

        #This boolean is true if the sender requests an ack from the destination
        #this will generally be true for data packets, so it defaults to True.
        self.ack_requested = ack_requested

        # The routingInfo tuple stores the linkID of the link that this packet
        # was sent on and the routing table of the router that sent the packet.
        # This is only used for determining routing tables, so the default
        # value is None.
        self.routingInfo = routingInfo
        
        # The sendTime stores the time that the packet was sent at. In the case
        # of acks, sendTime this should be the same as the time at which the 
        # initial packet was received.
        self.sendTime = sendTime

