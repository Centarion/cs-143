#!/usr/bin/python

'''
This file contains the Event Queue object.  The Event Queue stores the list of
events which we wish to happen in our system, in order of when they should
actually happen.
'''

class EventQueue(object):

    def __init__(self):
        '''Initializes the event queue.  Also initializes the timestamp of the
        system, and keeps track of it continuously.'''
        self.queue = []
        self.time = 0
        
    def is_empty(self):
        '''Returns if the internal queue is empty or not.'''
        return self.queue == []
        
    def get_time(self):
        '''Return the current timestamp of the queue.'''
        return self.time
        
    def set_time(self, newtime):
        '''Set the new timestamp of the queue.'''
        self.time = newtime
        
    def remove(self):
        '''Removes the first element from our queue, and returns it.'''
        return self.queue.pop(0)
        
    def add(self, time_delta, function):
        '''Given an amount of time in the future that code should be executed,
        and the function we wish to actually run, add this desired future code
        to the queue.'''
        
        # find the actual time when we'll run this code
        actual_time = self.time + time_delta
        
        # insert it into the list in the correct position
        # this could have been an O(log n) search to find the right position,
        # but since insertion is O(n) anyways, the O(n) search is fine
        i = 0
        while i < len(self.queue):
            # look at the timestamp of other items
            timestamp = self.queue[i][0]
            if actual_time < timestamp:
                break
            else:
                i += 1
                
        # i is now the index where we should insert the new element (if i is the
        # length of the list, it just goes at the end, as it should)
        self.queue.insert(i, (actual_time, function))
