#!/usr/bin/python

'''
This file is meant to parse input files and create the network we will be
simulating.
'''

import host_lib
import router_lib
import link_lib
import flow_lib

def parse_file(filename):
    '''Given an input file, parse it and generate the appropriate info.'''
    with open(filename, "r") as f:
        # Initialize dictionaries for hosts, routers, links, and flows
        hosts = {}
        routers = {}
        links = {}
        flows = {}
        
        # Be prepared for set of interesting links
        interest = None
        
        # Initialize time limit as negative to distinguish nonexistent limit
        limit = -1
        
        for line in f:
            # Remove newlines at end
            if line[-1] == "\n":
                line = line[:-1]
                
            # If there's a comment symbol present, ignore it and all things
            # following it
            if "#" in line:
                line = line.split("#", 1)[0]
                
            # If we have a blank line (or, by the above piece of code, a line
            # which is just a comment), ignore it and move on
            if line == "":
                continue
                
            # Split by tabs - separate the descriptor col from the value cols
            cols = line.split("\t")
            desc = cols[0]
            vals = cols[1:]
            # Parse each type of line
            if desc == "hosts":
                # This is the list of hosts
                # Store them, each initially with no connections
                for v in vals:
                    hosts[v] = set()
            elif desc == "routers":
                # This is the list of routers
                # Store them, each initially with no connections
                for v in vals:
                    routers[v] = set()
            elif desc == "link":
                # This is a link specification
                # vals[0] is link ID, vals[1,2] are endpoints, vals[3-5] are
                # extra link information.  All three pieces of extra info should
                # be floats.
                # First store info via link
                linkID = vals[0]
                
                # Recall that links are bi-directional in real life, but we
                # store each as a pair of one-directional objects.
                linkID_a = linkID + "_a"
                linkID_b = linkID + "_b"
                
                links[linkID_a] = (vals[1], vals[2], float(vals[3]),
                    float(vals[4]), float(vals[5]))
                links[linkID_b] = (vals[2], vals[1], float(vals[3]),
                    float(vals[4]), float(vals[5]))
                # Next store in each endpoint that this link exists (ensuring
                # that each node receives the link that starts at it)
                for (node, ID) in ((vals[1], linkID_a), (vals[2], linkID_b)):
                    if node in hosts:
                        hosts[node].add(ID)
                    else:
                        routers[node].add(ID)
            elif desc == "flow":
                # This is a flow specification
                # vals[0] is flow ID, vals[1,2] are endpoints, vals[3,4] are
                # extra flow information, vals[5] is flow control type.  The
                # endpoints are string host IDs, the extra information values
                # are floats, the flow control type is an integer or a string.

                # Deal with the extra info floats
                vals[3] = float(vals[3])
                vals[4] = float(vals[4])

                # Deal with the flow control type item
                try:
                    # if we can cast the string to an int, it's supposed to be
                    # an int
                    vals[5] = int(vals[5])
                except ValueError:
                    # if not, it should be a string and we just leave it be
                    # (any other errors should still be thrown as usual, hence
                    # this seemingly useless except clause)
                    pass
                    
                # now we can actually store the info
                flows[vals[0]] = tuple(vals[1:])
            elif desc == "interest":
                # This is the ignore specification
                # It lists IDs of objects for which we don't care about the
                # final data values
                interest = set(vals)
            elif desc == "limit":
                limit = float(vals[0])
            else:
                # Something's wrong
                raise ValueError('''incorrect line descriptor (%s)''' % desc +
                    ''' in file "%s"''' % filename)
                    
    # If we haven't explicitly listed any interesting links, then consider all
    # links as interesting
    if interest is None:
        # but ignore the _a and _b parts of the link IDs
        interest = set([linkID[:-2] for linkID in links.keys()])
    return ((hosts, routers, links, flows), interest, limit)

def initialize_objects(queue, host_data, router_data, link_data, flow_data):
    '''Initialize the objects of our system, given the data found by parse_file
    and the event queue holding it all together
    '''
    
    # First, initialize our various objects, storing them in dictionaries.
    # Note that, when we do so, any objects which are connected store each
    # other's IDs, rather than pointers to the objects themselves.
    hosts = {}
    routers = {}
    links = {}
    flows = {}
    for (h, info) in host_data.iteritems():
        hosts[h] = host_lib.Host(h, queue, info)
    for (r, info) in router_data.iteritems():
        routers[r] = router_lib.Router(r, queue, info)
    for (link, info) in link_data.iteritems():
        links[link] = link_lib.Link(link, queue, info)
    for (f, info) in flow_data.iteritems():
        flows[f] = flow_lib.Flow(f, queue, info)
    
    # Once all objects have been created, we can replace stored IDs with
    # pointers to the objects themselves. (See the problem noted above)
    for h in hosts:
        # Hosts note what links they connect to
        hosts[h].include_links(links)
    for r in routers:
        # Routers also note what links they connect to
        routers[r].include_links(links)
    for link in links:
        # Links store their endpoints (can be hosts or routers)
        links[link].include_endpoints(dict(hosts.items() + routers.items()))
    for f in flows:
        # Flows store their source and destination (both hosts)
        flows[f].include_endpoints(hosts)
        
    # Our objects have now been properly initialized
    return (hosts, routers, links, flows)
