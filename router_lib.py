# !/usr/bin/python

'''
This file contains the Router object and its methods.
'''

# The number of seconds in between scheduled broadcasts.
INTRVL = 1.

import packet_lib

class Router(object):

    def __init__(self, nodeID, queue, info):
        '''Initializes the router object.  Is given the router's ID (a string),
        the queue object, and info relating to the router (a set of link ID 
        strings, representing the links which start at our router).'''
        self.nodeID = nodeID
        # The links our node feeds into
        self.linkIDs = list(info)
        
        # Routing table
        # The nodeIDs serve as keys.  The value associated with a nodeID is a 
        # tuple of which the first element is the distance from this router to
        # that node and the second element is which link to use to get the 
        # associated distance
        self.table = {nodeID : (0, None)}
        
        # Set up our reference to the event queue
        self.event_queue = queue     
        
        # A set of the hosts that the router is directly connected to created
        # by checking the source of acks received in response to routing info
        self.hosts = set([])
        
    def include_links(self, links):
        '''Given the links of our system (a dictionary, with link ID strings as
        keys and the corresponding link objects as values), store references to
        the relevant links in our object.'''
        self.links = {}
        for ID in self.linkIDs:
            self.links[ID] = links[ID] 

    def send(self, packet, linkID=None):
        ''' Given a packet, send it along the link associated with its 
        destination. If linkID is specified, use linkID to index into the
        routers dict of links.'''
        
        # Get the proper linkIndex
        if not linkID:
            linkID = self.table[packet.header][1]
        
        # send packet into the appropriate link
        self.links[linkID].queue(packet)
            
    def receive(self, packet):
        '''Given a packet, process it and send it accross the link that will
        get it to the proper destination as quickly as possible'''
        
        # Use this to determine if this packet updated the routing table
        tableUpdated = False
        oldTab = {}
        
        for key in self.table:
            oldTab[key] = (self.table[key][0], self.table[key][1])
        
        # If this is a routing info packet, update routing info
        if (packet.type == "routingInfo"):
            # Deal with the packet like a routing info packet
            self._dealWithRoutingInfo(packet)              
            
        
        # If the packet is an ack with routing info, update the routing table
        elif (packet.type == "ack" and packet.header == self.nodeID):
            # Deal with the packet like an ack meant for this router
            self._dealWithAck(packet)
                                                   
        # If the router knows where to send the packet, send it.
        elif packet.header in self.table:
            self.send(packet)
           
        # Otherwise, we don't know what to do with the packet to do with it,
        # so don't do anything; just let the packet be dropped.
        
        # Check if the routing table has been changed
        for key in self.table:
            if (key not in oldTab or self.table[key][1] != oldTab[key][1]):
                tableUpdated = True
            
        # If the table was updated and the flows haven't started yet,
        # rebroadcast routing info.
        if tableUpdated and self.event_queue.get_time() <= 0.5:
            self.broadcast(True)
    
    def _dealWithRoutingInfo(self, packet):
        '''This is a method used only internally by routers to deal with
        routing info packets received'''
        linkID, otherTable = packet.routingInfo
            
        # First, find the link's ID from this router's side
        myLink = linkID[:-2] + {"_a" : "_b", "_b" : "_a"}[linkID[-2:]]
        
        # Next, send the ack so that the other router can compute the
        # link weight from its end
        p = packet_lib.Packet("ack", packet.p_id, packet.source, 
                  self.nodeID, self.event_queue.get_time(), 
                  packet.flow, 64, False, 
                  myLink) #All ack packets are 64 B
        self.send(p, myLink)
        
        # Update this table if possible       
        if (packet.source in self.table):
            # Get the linkID to get to the router that sent this table 
            # and its weight
            linkWeight, myLink = self.table[packet.source]
            for key in otherTable:
                # If a node is not already in this table or is in the table
                # and reachable more quickly based on new info, update the 
                # table. However, don't change the info for hosts that this
                # router is directly connected to
                # Also don't change the link if doing so would create a loop.
                if (key not in self.hosts):
                    if (key not in self.table or \
                        (self.table[key][0] > (linkWeight + otherTable[key][0]) and \
                         myLink[:-2] != otherTable[key][1][:-2])):
                        self.table[key] = (linkWeight + otherTable[key][0], myLink)
                        
                    # If the time taken needs to be updated, update
                    elif (self.table[key][0] < (linkWeight + otherTable[key][0])
                          and self.table[key][1] == myLink):
                        self.table[key] = (linkWeight + otherTable[key][0], myLink)
    
    def _dealWithAck(self, packet):
        '''This is a method only used internally in routers to deal with acks
        that are meant for this router'''
        host = packet.source
        linkID = packet.routingInfo

        # Get the link ID from this side
        linkID = linkID[:-2] + {"_a" : "_b", "_b" : "_a"}[linkID[-2:]]
        
        # Get the time that the original packet was sent at from the
        # packet ID
        linkTime = float(packet.p_id[2:])
        
        # If the ack is from a host, save the host as being directly
        # connected to the router.
        if (host[0] == "H"):
            self.hosts.add(host)
        
        # Calculate a link weight based on time it took for the inital
        # packet to arrive.
        linkWeight = packet.sendTime - linkTime

        # If the host that this ack came from is not in the table, add it.
        if (host not in self.table):
                self.table[host] = (linkWeight, linkID)
        
        # Get the old link weight for the link this ack corresponds to
        oldTime = self.table[host][0]        
       
        # if the linkWeight has changed, update
        if (oldTime != linkWeight): 
            # Set the new linkWeight for this link      
            for node in self.table:
                # If getting to this node requires using the link that 
                # this ack came from, update it
                if(self.table[node][1] == linkID or \
                    node == host and self.table[node][0] >= linkWeight):
                    temp = self.table[node][0]
                    # subtract the old linkWeight from the old time it took
                    # to get to that node and add the new linkWeight
                    self.table[node] = (temp - oldTime + linkWeight, linkID)
     
    def broadcast(self, updateCall=None):
        '''Broadcasts routing info'''
        # Get the time that this packet will be sent at
        broadcastTime = self.event_queue.get_time()
        
        # Generate an id for the packet that includes the time it was sent at
        p_id = "RI" + str(broadcastTime)

        for ID in self.linkIDs:
            info = (ID, self.table)
            p = packet_lib.Packet("routingInfo", p_id, "routingInfo", 
                                  self.nodeID, broadcastTime, None, 64, True,
                                  info) # All non-data packets are 64 bytes
            self.send(p, ID)
            
        # If this is a scheduled broadcast, schedule the next broadcast
        if (not updateCall):
            self.event_queue.add(INTRVL, lambda: self.broadcast())

            
    def kick_off(self):
        '''Method called by the controller to initiate building the
        routing table'''
        # Broadcast routing info to all links
        self.event_queue.add(0, lambda:self.broadcast())