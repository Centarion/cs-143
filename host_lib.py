#!/usr/bin/python

'''
This file contains the Host object and its methods.
'''
import packet_lib

class Host(object):
    
    def __init__(self, nodeID, queue, info):
        '''Initializes the node object.  Is given the node's ID (a string), the
        queue object, and info relating to the node (a set of link ID strings, 
        representing the links which start at our node).'''
        self.nodeID = nodeID
        #The links our node feeds into
        self.linkIDs = list(info)
        
        #Set up our reference to the event queue
        self.event_queue = queue
        
        #Keep stats for outpt
        self.packets_sent = 0
        self.packets_received = 0
        
    def include_links(self, links):
        '''Given the links of our system (a dictionary, with link ID strings as
        keys and the corresponding link objects as values), store references to
        the relevant links in our object.'''
        self.links = []
        for ID in self.linkIDs:
            self.links.append(links[ID])

    def send(self, p_id, target, flow):
        '''Given a target hostID constructs a packet and pushes it into 
        the link. Supplied some metadata for the packet including its p_id and 
        flow owner.'''
        #first make our packet
        p = packet_lib.Packet("data", p_id, target, self.nodeID, self.event_queue.get_time(), flow, 1024) #All data packets are 1 KB

        #send packet into our 1 link
        self.links[0].queue(p)
        self.packets_sent += 1

    def receive(self, packet):
        '''Given a packet, processes it and adds it to the packets list,
        also sends a ack packet'''
        if packet.type == "data":
            self.packets_received += 1
        elif packet.type == "ack":
            #We got an ack, inform the correct flow
            packet.flow.got_ack(packet.p_id)

        #Send ack if the packet has ack_requested set to true
        if packet.ack_requested:
            p = packet_lib.Packet("ack", packet.p_id, packet.source, 
                                  self.nodeID, self.event_queue.get_time(), 
                                  packet.flow, 64, False, 
                                  self.linkIDs[0]) #All ack packets are 64 B
            self.links[0].queue(p)
    
    def get_stats(self):
        '''
        Called by data output to get information about the node
        Returns a tuple (packets_sent, packets_received)
        '''
        return (self.packets_sent, self.packets_received)
        
        
