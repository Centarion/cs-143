#!/usr/bin/python

'''
This file is meant to collect measured data from our system, and create the
outputs we are interested in.
'''

import os, os.path

# Set up a container to store the aggregate data of our system
aggregate_data = []

# Keep track of the last timestamp (initially 0)
last_timestamp = 0

# How many attributes do we have?
num_attributes = 9

# List all attributes we'll be using in our system
# Also note the index of each attribute in our eventual datalist
attributes = {"link": {"link_buffer_occupancy": 0, "link_packet_loss": 1,
                       "link_flow_rate": 2},
              "flow": {"flow_send_rate": 3, "flow_receive_rate": 4,
                       "flow_packet_rtt": 5, "flow_window_size": 6},
              "host": {"host_send_rate": 7, "host_receive_rate": 8}}
              
# Note the ones we'll have to convert to rates
rates = set(["link_flow_rate", "flow_send_rate", "flow_receive_rate",
             "host_send_rate", "host_receive_rate"])

# Note the ones we should convert to differentials
diff = set(["link_packet_loss"])

# keep track of the indices for these special items, along with the remaining
# indices (which are just normal items)
rate_indices = set()
diff_indices = set()
normal_indices = set(range(num_attributes))
for att_dict in attributes.values():
    for (att, ind) in att_dict.iteritems():
        if att in rates:
            rate_indices.add(ind)
            normal_indices.remove(ind)
        elif att in diff:
            diff_indices.add(ind)
            normal_indices.remove(ind)
            

def find_proper_dict(datalist, index):
    '''Given a datalist and an index, find the dictionary with the relevant
    values for that index.  In particular, the datalist will either directly
    contain the dictionary at that index, or will have another index - this one
    for the correct entry in the aggregate_data collection where we can find the
    desired dictionary.  Also returns whether we actually had a dictionary
    (True) or just an index pointing to another dictionary (False).'''
    value = datalist[index]
    if type(value) is int:
        # we're given an index to the right place in aggregate_data
        return aggregate_data[value][2][index].copy(), False
    else:
        # we're given just the dictionary itself
        return value.copy(), True

def get_stats(timestamp, hosts, routers, links, flows, interest):
    '''This function takes as input:
    
    timestamp    - Float value, denoting the current timestamp of the system
    hosts        - Dictionary mapping strings (host IDs) to objects (hosts),
                   listing all hosts of our system
    routers      - Dictionary mapping strings (router IDs) to objects (routers),
                   listing all routers of our system
    links        - Dictionary mapping strings (host IDs) to objects (links),
                   listing all links of our system
    flows        - Dictionary mapping strings (flow IDs) to objects (flows),
                   listing all flows of our system
    interest     - Set listing the strings (link IDs) relating to links which we
                   care about, and want data from
    
    It can then, for each object in our system, collect the at-this-moment data
    that we desire, in order to aggregate it all together.  Because this
    function is called sporadically (only when events actually occur in the
    system), we note data before each action has taken place.  In doing so, we
    can note the ranges of time during which our stats do *not* change - as the
    previous timestamp recorded will be when the last action started and ended,
    and that "end" property is the last time any data changed.
    '''

    # note the last timestamp of our system
    global last_timestamp, aggregate_data
    
    # We will construct a 3-tuple of information, and add it to the end of our
    # aggregate data list.
    # [0] last_timestamp
    # [1] timestamp
    # [2] The actual data we've gathered
    
    # Our datalist will contain one dictionary for each attribute we care about
    datalist = []
    for i in range(num_attributes):
        datalist.append({})
    
    # From links, we need buffer occupancy, packet loss, and flow rate
    # For flow rate, we'll actually just store the total data throughput, and
    # later convert to flow rate
    for (linkID, obj) in links.iteritems():
        # only check interesting links (recall links are actually bidirectional,
        # so ignore the _a or _b at the end)
        if linkID[:-2] not in interest:
            continue
            
        buffer_occ, packet_loss, data_through = obj.get_stats()
        
        # store the data in our aggregate collection
        for (name, attribute) in (("link_buffer_occupancy", buffer_occ),
                                  ("link_packet_loss", packet_loss),
                                  ("link_flow_rate", data_through)):
            datalist[attributes["link"][name]][linkID] = attribute


    # From flows, we need send/receive rate and packet rtt
    for (flowID, obj) in flows.iteritems():
            
        # get the data in question for this flow
        packets_sent, packets_received, packet_rtt, window_size = obj.get_stats()
        
        # store the data in our aggregate collection
        for (name, attribute) in (("flow_send_rate", packets_sent),
                                  ("flow_receive_rate", packets_received),
                                  ("flow_packet_rtt", packet_rtt),
                                  ("flow_window_size", window_size)):
            datalist[attributes["flow"][name]][flowID] = attribute
            
            
    # From hosts, we need send/receive rate
    for (hostID, obj) in hosts.iteritems():
            
        # get the data in question for this flow
        packets_sent, packets_received = obj.get_stats()
        
        # store the data in our aggregate collection
        for (name, attribute) in (("host_send_rate", packets_sent),
                                  ("host_receive_rate", packets_received)):
            datalist[attributes["host"][name]][hostID] = attribute
            
    # Having constructed our datalist, let's see if there's any redundant info
    # This can be done by comparing each dictionary to the dictionary held at
    # the previous datalist, which is currently the last element of the
    # aggregate_data list
    if len(aggregate_data) > 0:
        previous_datalist = aggregate_data[-1][2]
        for i in range(num_attributes):
            current_dict = datalist[i]
            previous_dict, isDict = find_proper_dict(previous_datalist, i)
            if current_dict == previous_dict:
                # if we have a match, store an index rather than holding the
                # dictionary
                if isDict:
                    # the dictionary was actually there, so start storing that index
                    # as the reference
                    refer_index = len(aggregate_data) - 1
                else:
                    # we found another index, so just keep that same index
                    refer_index = previous_datalist[i]
                    
                # either way, store that new index in our dictionary
                datalist[i] = refer_index

    # add this new data to our aggregate collection
    gathered_data = (last_timestamp, timestamp, datalist)    
    aggregate_data.append(gathered_data)

    # now that we're done storing data, we can update last_timestamp to be the
    # current timestamp
    last_timestamp = timestamp
    
    # If we want to do real-time analysis of the data, here this is the place to
    # do it
    pass
    

def get_data_timeslice(start, end, index = 0):
    '''Given a start time and an end time, get the timeslice of collected data
    over that time period.  Takes an optional index, which notes where in the
    list of collected data we can start (to avoid redundantly checking earlier
    values).  Returns the data slice and the index where we ended.'''
    
    collected_data = []
    
    # loop starting at index, and make sure we can end as soon as we're done
    while index < len(aggregate_data):
        (s, e, vals) = aggregate_data[index]
        
        # Determine what we'll be recording from this timeslice
        
        if s == e:
            # data point is instantaneous
            # doesn't actually give us anything useful
            index += 1
            continue
            
        if e <= start:
            # not in our range (too early)
            index += 1
            continue
        elif end <= s:
            # not in our range (too late)
            break
            
        # now we're in our range
        if s <= start < e:
            # start midway through timeslice
            t1 = start
            # could end on either side of timeslice
            t2 = min(e, end)
        elif start < s and e <= end:
            # timeslice within our period
            t1 = s
            t2 = e
        elif start <= end < e:
            # end midway through timeslice
            t1 = s
            t2 = end
        else:
            # something's wrong
            print start,end,s,e
            raise ValueError("average data timeslice invalid")
            
        # we now have t1 and t2 as the times we care about for these values
        collected_data.append((t1, t2, vals))
        
        # if we're now done, break; otherwise increment the index by one and
        # keep going
        if end <= e:
            break
        else:
            index += 1
        
    # once we're out of our loop, we should be done timeslicing
    return (collected_data, index)


def get_average_data(start, end, index = 0):
    '''Given a start time and an end time, get the average values of each set
    of data which we care about over that time period.  Takes an optional index,
    which notes where in the list of collected data we can start (to avoid
    redundantly checking earlier values).'''
    
    # get the part of the data we actually care about
    collected_data, index = get_data_timeslice(start, end, index)
    
    # start off our average data list of dictionaries
    avg_data = []
    for i in range(num_attributes):
        # use a sample from the collected data to note the keys we care about
        # (make sure we actually have the right valdict, and not an index
        # replacement)
        valdict = find_proper_dict(collected_data[0][2], i)[0]
        
        # for each key in this dictionary, let's start off with a blank slate
        avg_data.append(dict([(key, 0) for key in valdict.keys()]))
    
    # walk through our collected data and start aggregating
    for (s, e, vals) in collected_data:
        # only for normal (non rate or diff) attributes do we care about every
        # last data point
        for i in normal_indices:
            # make sure we actually have the right valdict, and not an index
            # replacement
            valdict = find_proper_dict(vals, i)[0]

            for (objID, value) in valdict.iteritems():
                avg_data[i][objID] += value * (e - s)
                
    # for rates and diffs, we only care about the first and last points
    vals_start = collected_data[0][2]
    vals_end = collected_data[-1][2]
    
    # both rates and diffs start with val_end - val_start
    for i in rate_indices.union(diff_indices):
        # make sure we actually have the right valdicts, and not an index
        # replacement
        end_i = find_proper_dict(vals_end, i)[0]
        start_i = find_proper_dict(vals_start, i)[0]
        
        for objID in avg_data[i]:
            avg_data[i][objID] = end_i[objID] - start_i[objID]
        
    # in the case of rates and normal indices, we're then dividing by total time
    # to get an average
    for i in normal_indices.union(rate_indices):
        for objID in avg_data[i]:
            avg_data[i][objID] /= float(end - start)
            
    # now we can return out average data (and the index from our timeslice)
    return (avg_data, index)


    
def time_trace(sample):
    '''Generates a time trace of each aspect of our data, given a sampling rate
    (measured in seconds).'''
    
    # get overall start and end times of system
    start = aggregate_data[0][0]
    end = aggregate_data[-1][1]

    # time traces are more efficient if we don't recheck the data list for old
    # timeslices (since we're walking in order); start at the beginning of our
    # data
    index = 0

    # count how many traces we've done
    counter = 0

    # start walking through the timerange of our system
    # each slice goes from previous_time to current_time
    traces = []
    current_time = start + sample
    while current_time - sample < end:
        counter += 1
        # every 50 traces, print out the current trace we're looking at, so that
        # the person looking at the code knows it's not just stuck forever
        if counter % 50 == 0:
            print current_time
        previous_time = current_time - sample
        data, index = get_average_data(previous_time, current_time, index)
        traces.append((current_time, data))
        current_time += sample
    return traces
    
def overall_average():
    '''Generates an overall average of each aspect of our data, and returns it
    in a new dictionary (with structure based on aggregate_data dictionary)'''

    # starting period is just the beginning of time
    # ending period is just the end of time
    start = aggregate_data[0][0]
    end = aggregate_data[-1][1]
    
    # we don't care about the index in this case
    return get_average_data(start, end)[0]
    
def output_overall_average(outfile, sep="\t"):
    '''Runs the overall average code, and outputs to a specified outfile.  Uses
    a separating character for the output file, chosen by the user (defaults to
    the tab character).'''
    
    # Creates the directory specified in the output file
    dirname = "/".join(outfile.split("/")[:-1])
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
        
    # Actually gets the overall average data
    overall = overall_average()
    
    # Be ready to store data based on attribute, rather than by index in the
    # aggregate datalist
    results = {}
    for (key, valdict) in attributes.iteritems():
        # we only want the attribute names, not their indices
        vals = valdict.keys()
        for v in vals:
            results[v] = {}
            
    # Walk through the average data and put it into our new storage
    # dictionary
    for key in attributes:
        # key is "link", "host", or "flow"
        for att in attributes[key]:
            # att is now an attribute name
            
            # get the data for this attribute
            objdict = overall[attributes[key][att]]
            
            # Walk through the individual objects which stored data for this
            # attribute
            for (obj, val) in objdict.iteritems():
                # we only have a single piece of data for each object; let's
                # store it, but after the attribute name and the object name
                results[att][obj] = [att, obj, str(val)]
                
    # Once we've walked through all the data, write it to our output file
    with open(outfile, "w") as f:
        for att in results:
            for obj in results[att]:
                f.write(sep.join(results[att][obj]) + "\n")
    
def output_time_trace(outfile, sample, sep="\t"):
    '''Runs the time trace code, and outputs to a specified outfile.  Takes an
    argument 'sample' to determine the sampling rate of the time trace (measured
    in seconds).  Uses a separating character for the output file, chosen by the
    user (defaults to the tab character).'''
    
    # Creates the directory specified in the output file
    dirname = "/".join(outfile.split("/")[:-1])
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
        
    # Actually creates the time trace data, given our sampling rate
    traces = time_trace(sample)
    
    # Be ready to store data based on attribute, rather than by index in the
    # aggregate datalist
    results = {}
    for (key, valdict) in attributes.iteritems():
        # we only want the attribute names, not their indices
        vals = valdict.keys()
        for v in vals:
            results[v] = {}
            
    # Walk through the time trace data and put it into our new storage
    # dictionary
    for (_, slicelist) in traces:
        # time doen't actually matter here; we just want the slicelist
        for key in attributes:
            # key is "link", "host", or "flow"
            for att in attributes[key]:
                # att is now an attribute name
                
                # get the data for this attribute at this particular timeslice
                objdict = slicelist[attributes[key][att]]
                
                # Walk through the individual objects which stored data for this
                # attribute
                for (obj, val) in objdict.iteritems():
                    # if we haven't gotten data yet for this object, it means
                    # we're at the beginning, and so we start off our data
                    # with the attribute name, the object name, and the length
                    # of time we use to sample
                    if obj not in results[att]:
                        results[att][obj] = [att, obj, str(sample)]
                    
                    # once we're guaranteed to be ready to accept data for this
                    # object, add in the current value
                    results[att][obj].append(str(val))
                    
    # Once we've walked through all the data, write it to our output file
    with open(outfile, "w") as f:
        for att in results:
            for obj in results[att]:
                f.write(sep.join(results[att][obj]) + "\n")
